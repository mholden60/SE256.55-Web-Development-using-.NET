﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Tables.aspx.cs" Inherits="badBurger.Tables" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- Content to be Displayed on screen to ensure the use of page is working --%>
    THIS IS MY Tables PAGE
    <div class="table-responsive">
        <div class="col-md-12">
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:SE256_HoldenConnectionString %>" SelectCommand="tables_getall" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="tbl_id" DataSourceID="SqlDataSource1" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical">
                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                <RowStyle HorizontalAlign="Center" BackColor="#EEEEEE" ForeColor="Black" />
                <AlternatingRowStyle BackColor="#DCDCDC" />
                <Columns>
                    <asp:HyperLinkField DataNavigateUrlFields="tbl_id" DataNavigateUrlFormatString="~/Admin/Tables/Table/{0}" DataTextField="tbl_name" HeaderText="Table Name"/>                    
                    <asp:BoundField DataField="tbl_desc" HeaderText="Table Description" SortExpression="tbl_desc" />
                    <asp:BoundField DataField="tbl_seat_cnt" HeaderText="Seat Count" SortExpression="tbl_seat_cnt" />
                    <asp:CheckBoxField DataField="tbl_active" HeaderText="Active" SortExpression="tbl_active" />
                </Columns>

                <RowStyle BackColor="White" ForeColor="#330099" />
                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#0000A9" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#000065" />
            </asp:GridView>
        </div>
    </div>
</asp:Content>

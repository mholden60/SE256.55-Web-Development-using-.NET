﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Sections.aspx.cs" Inherits="badBurger.Sections" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  This is my Section Page
    <div class="table-responsive">
        <div class="col-sm-12">
            <asp:SqlDataSource ID="sdsSections" runat="server" ConnectionString="<%$ ConnectionStrings:SE256_HoldenConnectionString %>" SelectCommand="sections_getall" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="sect_id" DataSourceID="sdsSections">
                <Columns> 
                    <asp:HyperLinkField DataNavigateUrlFields="sect_id" DataNavigateUrlFormatString="~/Admin/Section/{0}" DataTextField="sect_name" HeaderText="Section Items" />
                    <asp:BoundField DataField="sect_desc" HeaderText="Description" SortExpression="sect_desc" />
                    <asp:CheckBoxField DataField="sect_active" HeaderText="Active" SortExpression="sect_active" />

                   

                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>

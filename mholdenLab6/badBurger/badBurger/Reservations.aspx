﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BadBurger.Master" AutoEventWireup="true" CodeBehind="Reservations.aspx.cs" Inherits="badBurger.Reservations" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- Content to be Displayed on screen to ensure the use of page is working --%>
    THIS IS MY RESERVATIONS PAGE

    <div class="table-responsive">
        <div class="col-sm-12">
            <asp:GridView ID="gvReservation" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="res_id" DataSourceID="SqlDataSource1">
                <Columns>
<asp:BoundField DataField="guest_id" HeaderText="Guest" SortExpression="guest_id"></asp:BoundField>
                    <asp:BoundField DataField="user_id" HeaderText="User" SortExpression="user_id" />
<asp:BoundField DataField="res_date" DataFormatString="{0: MMM dd,yyyy}" HeaderText="Date" SortExpression="res_date"></asp:BoundField>
                    <asp:BoundField DataField="res_time" HeaderText="Time" SortExpression="res_time" />
                    <asp:BoundField DataField="res_guest_cnt" HeaderText="Guest Count" SortExpression="res_guest_cnt" />
                    <asp:BoundField DataField="res_spec_req" HeaderText="Requests" SortExpression="res_spec_req" />
                    <asp:HyperLinkField DataNavigateUrlFields="res_id" DataNavigateUrlFormatString="~/Reservation/{0}" DataTextField="res_id" HeaderText="ID" />
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:SE256_HoldenConnectionString %>" SelectCommand="reservations_getall" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </div>
    </div>
</asp:Content>

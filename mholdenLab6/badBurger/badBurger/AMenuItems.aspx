﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AMenuItems.aspx.cs" Inherits="badBurger.MenuItems" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- Content to be Displayed on screen to ensure the use of page is working --%>
    THIS IS MY MenuItems PAGE


    <div class="table-responsive">
        <div class="col-sm-12">
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="item_id" DataSourceID="sdsMenuItems">
                <RowStyle HorizontalAlign="Center" />
                <Columns>
                    <asp:HyperLinkField DataNavigateUrlFields="item_id" DataNavigateUrlFormatString="~/Admin/AMenuItems/AMenuItem/{0}" DataTextField="item_name" HeaderText="Menu Items" />
                    <asp:BoundField DataField="item_name" HeaderText="Name" SortExpression="item_name" />
<asp:BoundField DataField="item_desc" HeaderText="Description" SortExpression="item_desc"></asp:BoundField>
<asp:BoundField DataField="item_allergens" HeaderText="Allergens" SortExpression="item_allergens"></asp:BoundField>
                    <asp:BoundField DataField="item_price" HeaderText="Price" SortExpression="item_price" />
                    <asp:CheckBoxField DataField="item_gluten_free" HeaderText="Gluten Free" SortExpression="item_gluten_free" />
                    <asp:CheckBoxField DataField="item_active" HeaderText="Active" SortExpression="item_active" />
                </Columns>
                <PagerSettings Position="Bottom" PageButtonCount="2" Mode="NextPrevious" NextPageText="Next" PreviousPageText="Previous"/>
            </asp:GridView>
            <asp:SqlDataSource ID="sdsMenuItems" runat="server" ConnectionString="<%$ ConnectionStrings:SE256_HoldenConnectionString %>" SelectCommand="menu_items_getall" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </div>
            </div>
</asp:Content>

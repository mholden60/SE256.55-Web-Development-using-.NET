﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="badBurger.User" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--************************************Forms*****************************************************************************************--%>THIS IS MY USER PAGE
    <h1>User</h1>
    <div class="container-fluid">
        <div class="row">

            <div class="col-sm-4">
                <%--************************************Forms*****************************************************************************************--%>
                <div class="form-group">
                    <asp:Label ID="lblFname" runat="server" Text="First Name"></asp:Label>
                    <asp:TextBox ID="txtFname" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblLname" runat="server" Text="Last Name"></asp:Label>
                    <asp:TextBox ID="txtLname" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblAddress" runat="server" Text="Address 1"></asp:Label>
                    <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblAddress2" runat="server" Text="Address 2"></asp:Label>
                    <asp:TextBox ID="txtAddress2" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblCity" runat="server" Text="City"></asp:Label>
                    <asp:TextBox ID="txtCity" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblState" runat="server" Text="State"></asp:Label>
                    <asp:DropDownList ID="ddlStates" runat="server" AppendDataBoundItems="True" DataSourceID="sqlStates" DataTextField="state_Full_name" DataValueField="state_id" ForeColor="Black">
                    <asp:ListItem>Please Choose a State</asp:ListItem>
                   </asp:DropDownList>
                    <asp:SqlDataSource ID="sqlStates" runat="server" ConnectionString="<%$ ConnectionStrings:SE256_HoldenConnectionString %>" SelectCommand="states_getall" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblZip" runat="server" Text="Zip"></asp:Label>
                    <asp:TextBox ID="txtZip" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblPword" runat="server" Text="Password"></asp:Label>
                    <asp:TextBox ID="txtPword" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblCword" runat="server" Text="Confirm Password"></asp:Label>
                    <asp:TextBox ID="txtCword" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblCemail" runat="server" Text="Confirm Email"></asp:Label>
                    <asp:TextBox ID="txtCemail" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblPhone" runat="server" Text="Phone"></asp:Label>
                    <asp:TextBox ID="txtPhone" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblActive" runat="server" Text="Is Active"></asp:Label>
                    <asp:CheckBox ID="cbActive" runat="server" CausesValidation="True" />
                </div>
                <div class="form-group">
                    <asp:Button ID="btnUpdate" runat="server" Text="Update" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false" PostBackUrl="~/Default.aspx" />
                </div>
                <div>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" BorderStyle="Solid" />
                    <asp:RequiredFieldValidator ID="rfvFname" runat="server" ErrorMessage="First Name is Required" ControlToValidate="txtFname" Display="None"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rfvLname" runat="server" ErrorMessage="Last Name is Required" ControlToValidate="txtLname" Display="None"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rfvAddress" runat="server" ErrorMessage="Address is Required" ControlToValidate="txtAddress" Display="None"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rfvState" runat="server" ErrorMessage="State is Required" InitialValue="Please Choose a State" Display="None" ControlToValidate="ddlStates"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rfvCity" runat="server" ErrorMessage="City is Required" ControlToValidate="txtCity" Display="None"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rfvZip" runat="server" ErrorMessage="Zip is Required" ControlToValidate="txtZip" Display="None"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ErrorMessage="Password is Required" ControlToValidate="txtPword" Display="None"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rfvCword" runat="server" ErrorMessage="Confirm Password is Required" ControlToValidate="txtCword" Display="None"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="Email is Required is Required" ControlToValidate="txtEmail" Display="None"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rfvCmail" runat="server" ErrorMessage="Confirm Email is Required" ControlToValidate="txtCemail" Display="None"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rfvPhone" runat="server" ErrorMessage="Phone is Required" ControlToValidate="txtPhone" Display="None"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="cvAvtive" runat="server" ErrorMessage="Please check Active Box" OnServerValidate="cvActive_ServerValidate" Display="None"></asp:CustomValidator>
                     <asp:RegularExpressionValidator ID="validCmail" runat="server" ErrorMessage="Invaild Confirm Email" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$" ControlToValidate="txtCemail" Display="None"></asp:RegularExpressionValidator><br />
                    <asp:RegularExpressionValidator ID="validEmail" runat="server" ErrorMessage="Invaild Email" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$" ControlToValidate="txtEmail" Display="None"></asp:RegularExpressionValidator><br />
                    <asp:CompareValidator ID="compareEmail" runat="server" ErrorMessage="Email not Identical" Display="None" ControlToCompare="txtCemail" ControlToValidate="txtEmail"></asp:CompareValidator>
                    <asp:CompareValidator ID="comparePassword" runat="server" ErrorMessage="Password not Identical"  Display="None" ControlToCompare="txtCword" ControlToValidate="txtPword"></asp:CompareValidator>
                    <asp:RegularExpressionValidator ID="ValidZip" runat="server" ErrorMessage="Invalid Zip Code" ControlToValidate="txtZip" ValidationExpression="\d{5}-?(\d{4})?$" Display="None"></asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator ID="ValidPhone" runat="server" ErrorMessage="Enter Valid Phone Number" ControlToValidate="txtPhone" ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$" ></asp:RegularExpressionValidator>
                    <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer" ControlToValidate="txtZip" ErrorMessage="Zip in Numbers Only" Display="None" />
                    <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer" ControlToValidate="txtPhone" ErrorMessage="Phone in Numbers Only" Display="None" />

                </div>
            </div>
        </div>
    </div>
</asp:Content>

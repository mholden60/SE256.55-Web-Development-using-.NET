﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="mholdenMid.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Signature Form</h1>
    <div class="container-fluid">
        <div class="row">
            <%--Drop Down List States--%>
            <div class="col-sm-4">
                <div class="form-group">
                    <asp:Label ID="lblState" runat="server" Text="State"></asp:Label>
                    <asp:DropDownList ID="ddlStates" runat="server" DataSourceID="sqlStates" AppendDataBoundItems="True" BackColor="Red" DataTextField="state_Full_name" DataValueField="state_id" ForeColor="Black">
                        <asp:ListItem Text="Select"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="sqlStates" runat="server" ConnectionString="<%$ ConnectionStrings:SE256_HoldenConnectionString %>" SelectCommand="states_getall" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                    
                </div>
               <%--Textbox Name--%>
                <div class="form-group">
                    <asp:Label ID="lblName" runat="server" Text="Name"></asp:Label>
                    <asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <%--Textbox Email--%>
                <div class="form-group">
                    <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <%--Textbox Confirm email--%>
                <div class="form-group">
                    <asp:Label ID="lblCmail" runat="server" Text="Confirm Email"></asp:Label>
                    <asp:TextBox ID="txtCmail" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <%--Textbox Comment--%>
                <div class="form-group">
                    <asp:Label ID="lblComments" runat="server" Text="Comments"></asp:Label>
                    <asp:TextBox ID="txtComments" runat="server" CssClass="auto-style2" Height="38px" Width="809px" Rows="5" TextMode="MultiLine"></asp:TextBox>
                </div>
                <%--Checkbox Agree--%>
                <div class="form-group">
                    <asp:Label ID="lblAgree" runat="server" Text="Agree"></asp:Label>
                    <asp:CheckBox ID="cbAgree" runat="server" Text="I agree and Sign this Petition" />
                </div>
                <%--Button Cancel/Submit--%>
                <div class="form-group">
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false" PostBackUrl="~/Default.aspx" />
                </div>
                <%--Validators--%>
                <div>
                    <asp:RequiredFieldValidator ID="rfvStates" runat="server" ErrorMessage="Please Select States" ControlToValidate="ddlStates" Display="None" InitialValue="Select"></asp:RequiredFieldValidator>
                    <asp:ValidationSummary ID="validSummary" runat="server" DisplayMode="List" ForeColor="Red" />
                  <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="Name is Required" ControlToValidate="txtName" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="Email is Required" ControlToValidate="txtEmail" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rfvCmail" runat="server" ControlToValidate="txtCmail" ErrorMessage="Confirm Email is Required" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rfvComment" runat="server" ControlToValidate="txtComments" ErrorMessage="Comment is Required" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="validCmail" runat="server" ErrorMessage="Invaild Confirm Email" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$" ControlToValidate="txtCmail" Display="None"></asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator ID="validEmail" runat="server" ErrorMessage="Invaild Email" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$" ControlToValidate="txtEmail" Display="None"></asp:RegularExpressionValidator>
                     <asp:CompareValidator ID="compareEmail" runat="server" ErrorMessage="Email not Identical" Display="None" ControlToCompare="txtCmail" ControlToValidate="txtEmail"></asp:CompareValidator>
                </div>
            </div>
        </div>
    </div>
    
</asp:Content>

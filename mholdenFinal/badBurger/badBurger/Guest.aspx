﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BadBurger.Master" AutoEventWireup="true" CodeBehind="Guest.aspx.cs" Inherits="badBurger.Guest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>GUEST</h1>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <asp:Label ID="lblFname" runat="server" Text="First Name: "></asp:Label>
                    <asp:TextBox ID="txtFname" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblLname" runat="server" Text="Last Name: "></asp:Label>
                    <asp:TextBox ID="txtLname" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblEmail" runat="server" Text="Email: "></asp:Label>
                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblCemail" runat="server" Text="Confirm Email: "></asp:Label>
                    <asp:TextBox ID="txtCemail" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblPword" runat="server" Text="Password: "></asp:Label>
                    <asp:TextBox ID="txtPword" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblCpword" runat="server" Text="Confirm Password: "></asp:Label>
                    <asp:TextBox ID="txtCpword" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblPhone" runat="server" Text="Phone: "></asp:Label>
                    <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false" PostBackUrl="~/Default.aspx" />
                </div>
            </div>
        </div>

        <asp:RequiredFieldValidator ID="rfvFname" runat="server" ErrorMessage="FIRST NAME REQUIRED!" ForeColor="Red" ControlToValidate="txtFname"></asp:RequiredFieldValidator><br />
        <asp:RequiredFieldValidator ID="rfvLname" runat="server" ErrorMessage="LAST NAME REQUIRED!" ForeColor="Red" ControlToValidate="txtLname"></asp:RequiredFieldValidator><br />
        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="EMAIL REQUIRED!" ForeColor="Red" ControlToValidate="txtEmail"></asp:RequiredFieldValidator><br />
        <asp:RequiredFieldValidator ID="rfvCemail" runat="server" ErrorMessage="CONFIRM EMAIL REQUIRED!" ForeColor="Red" ControlToValidate="txtCemail"></asp:RequiredFieldValidator><br />
        <asp:RequiredFieldValidator ID="rfvPword" runat="server" ErrorMessage="PASSWORD REQUIRED!" ForeColor="Red" ControlToValidate="txtPword"></asp:RequiredFieldValidator><br />
        <asp:RequiredFieldValidator ID="rfvCpword" runat="server" ErrorMessage="CONFIRM PASSWORD REQUIRED!" ForeColor="Red" ControlToValidate="txtCpword"></asp:RequiredFieldValidator><br />
        <asp:RequiredFieldValidator ID="rfvPhone" runat="server" ErrorMessage="PHONE NUMBER REQUIRED!" ForeColor="Red" ControlToValidate="txtPhone"></asp:RequiredFieldValidator><br />
        <asp:CompareValidator ID="cvEmail" runat="server" ErrorMessage="EMAILS MUST MATCH" ControlToCompare="txtEmail" ControlToValidate="txtCemail" ForeColor="Red"></asp:CompareValidator><br />
        <asp:CompareValidator ID="cvPword" runat="server" ErrorMessage="PASSWORDS MUST MATCH" ControlToCompare="txtPword" ControlToValidate="txtCpword" ForeColor="Red"></asp:CompareValidator><br />
        <asp:RegularExpressionValidator ID="ValidPhone" runat="server" ErrorMessage="ENTER VALID PHONE NUMBER" ControlToValidate="txtPhone" ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$" ForeColor="Red"></asp:RegularExpressionValidator><br />

    </div>

</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Optimization;
using System.Web.Routing;

namespace badBurger
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes(RouteTable.Routes);
        }
        //Routes to make the URL user friendly
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapPageRoute("Default", "Default", "~/Default.aspx");
            routes.MapPageRoute("Guest", "Guest", "~/Guest.aspx");
            routes.MapPageRoute("Lunchmenu", "LunchMenu", "~/LunchMenu.aspx");
            routes.MapPageRoute("Dinnermenu", "DinnerMenu", "~/DinnerMenu.aspx");
            routes.MapPageRoute("Reservations", "Reservations", "~/Reservations.aspx");
            routes.MapPageRoute("Reservation", "Reservation", "~/Reservation.aspx");
            //**************************************************************************
            routes.MapPageRoute("User", "Admin/User", "~/User.aspx");
            routes.MapPageRoute("Users", "Admin/Users", "~/Users.aspx");
            //*************************************************************************
            routes.MapPageRoute("Directions", "Directions", "~/Directions.aspx");
            routes.MapPageRoute("About", "About", "~/About.aspx");
            routes.MapPageRoute("Contact", "Contact", "~/Contact.aspx");
            routes.MapPageRoute("Login", "Login", "~/Login.aspx");
            routes.MapPageRoute("Forgotpassword", "ForgotPassword", "~/ForgotPassword.aspx");

            //Admin Routes
            routes.MapPageRoute("AMenuItem", "Admin/AMenuItem", "~/AMenuItem.aspx");
            //routes.MapPageRoute("PMenu", "Admin/AMenuItem/{0}", "~/AMenuItem.aspx");
            routes.MapPageRoute("AMenuItems", "Admin/AMenuItems", "~/AMenuItems.aspx");
            routes.MapPageRoute("Section", "Admin/Section", "~/Section.aspx");
            routes.MapPageRoute("Sections", "Admin/Sections", "~/Sections.aspx");
            routes.MapPageRoute("Table", "Admin/Table", "~/Table.aspx");
            routes.MapPageRoute("Tables", "Admin/Tables", "~/Tables.aspx");
            routes.MapPageRoute("ResMgmt", "Admin/ResMgmt", "~/ResMgmt.aspx");



            routes.MapPageRoute("item_id", "Admin/AMenuItem/{item_id}", "~/AMenuItem.aspx", false, new RouteValueDictionary { { "item_id", "-1" } });
            routes.MapPageRoute("sect_id", "Admin/Section/{sect_id}", "~/Section.aspx", false, new RouteValueDictionary { { "sect_id", "-1" } });
            routes.MapPageRoute("user_id", "Admin/User/{user_id}", "~/User.aspx", false, new RouteValueDictionary { { "user_id", "-1" } });
            routes.MapPageRoute("res_id", "Admin/Reservation/{res_id}", "~/Reservation.aspx", false, new RouteValueDictionary { { "res_id", "-1" } });
            routes.MapPageRoute("guest_id", "Admin/Guest/{guest_id}", "~/AMenuItem.aspx", false, new RouteValueDictionary { { "guest_id", "-1" } });
            routes.MapPageRoute("tbl_id", "Admin/Table/{tbl_id}", "~/Table.aspx", false, new RouteValueDictionary { { "tbl_id", "-1" } });


        }
    }
}
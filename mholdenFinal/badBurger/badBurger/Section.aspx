﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Section.aspx.cs" Inherits="badBurger.Section" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--************************************Forms*****************************************************************************************--%>THIS IS MY Section PAGE
      <%--************************************Forms*****************************************************************************************--%>
      <h1>SECTION</h1>
    <div class="container-fluid">
        <div class="row">

            <div class="col-sm-4">
                <%--************************************Forms*****************************************************************************************--%>
                <div class="form-group">
                    <asp:Label ID="lblName" runat="server" Text="Name"></asp:Label>
                    <asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblDesc" runat="server" Text="Describe"></asp:Label>
                    <asp:TextBox ID="txtDesc" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblActive" runat="server" Text="Is Active"></asp:Label>
                    <asp:CheckBox ID="cbActive" runat="server" CausesValidation="True" />
                </div>
                
                <div class="form-group">
                    <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false" PostBackUrl="~/Default.aspx" OnClick="btnCancel_Click"  />
                </div>
                <div>
                    <asp:ValidationSummary ID="validSummary" runat="server" BorderStyle="Solid" />
                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="Name is Required" ControlToValidate="txtName" Display="None"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rfvDesc" runat="server" ErrorMessage="Description is Required" ControlToValidate="txtDesc" Display="None"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="cvActive" runat="server" ErrorMessage="Please check Active Box" OnServerValidate="cvActive_ServerValidate" Display="None"></asp:CustomValidator>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

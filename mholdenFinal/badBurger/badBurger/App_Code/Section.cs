﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace badBurger.App_Code
{
    public class Section
    {
        #region Properties
        public int SectID { get; set; }
        public string SectName { get; set; }
        public string Description { get; set; }
        public Boolean Active { get; set; }
        #endregion

        #region Constructors
        public Section()
        {

        }
        public Section(int id)
        {
            DataTable dt = new DataTable();
            dt = GetSectionByID(id);  
            if(dt.Rows.Count>0)
            {
                this.SectID = Convert.ToInt32(dt.Rows[0]["sect_id"].ToString());
                this.SectName = dt.Rows[0]["sect_name"].ToString();
                this.Description = dt.Rows[0]["sect_desc"].ToString();
                this.Active = Convert.ToBoolean(dt.Rows[0]["sect_active"].ToString());
            }
        }
        #endregion
        #region Methodes/Functions
        private static DataTable GetSectionByID(int intID)
        {
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SE256_HoldenConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("sections_getbyid", cn);
            DataTable dt = new DataTable();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("sect_id", SqlDbType.VarChar).Value = intID;

            try
            {
                cn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception exc)
            {
                exc.ToString();
            }
            finally
            {
                cn.Close();
            }
            return dt;
        }
        public static bool InsertSection(Section sec)
        {
            bool blnSuccess = false;
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SE256_HoldenConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("sections_insert", cn);
            DataTable dt = new DataTable();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("sect_name", SqlDbType.VarChar).Value = sec.SectName;
            cmd.Parameters.Add("sect_desc", SqlDbType.VarChar).Value = sec.Description;
            cmd.Parameters.Add("sect_active", SqlDbType.VarChar).Value = sec.Active;
            

            try
            {
                cn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception exc)
            {
                exc.ToString();
            }
            finally
            {
                cn.Close();
            }
            return blnSuccess;
        }

        public static bool UpdateSection(Section sec)
        {
            bool blnSuccess = false;
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SE256_HoldenConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("sections_update", cn);
            DataTable dt = new DataTable();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("sect_id", SqlDbType.VarChar).Value = sec.SectID;
            cmd.Parameters.Add("sect_name", SqlDbType.VarChar).Value = sec.SectName;
            cmd.Parameters.Add("sect_desc", SqlDbType.VarChar).Value = sec.Description;
            cmd.Parameters.Add("sect_active", SqlDbType.VarChar).Value = sec.Active;


            try
            {
                cn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception exc)
            {
                exc.ToString();
            }
            finally
            {
                cn.Close();
            }
            return blnSuccess;
        }
        #endregion
    }
}
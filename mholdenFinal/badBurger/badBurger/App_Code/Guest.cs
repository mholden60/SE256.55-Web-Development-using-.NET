﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace badBurger.App_Code
{
    public class Guest
    {
        #region Properties
        public int ID { get; set; }
        public string Email { get; set; }
        public string First { get; set; }
        public string Last { get; set; }
        public string Salt { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        #endregion

        #region Constructors
        public Guest()
        {

        }
        
        public Guest(int id)
        {
            DataTable dt = new DataTable();
            dt = GetGuestByID(id);
            if(dt.Rows.Count > 0)
            {
                this.ID = Convert.ToInt32(dt.Rows[0]["guest_id"].ToString());
                this.Email = dt.Rows[0]["guest_email"].ToString();
                this.First = dt.Rows[0]["guest_first"].ToString();
                this.Last = dt.Rows[0]["guest_last"].ToString();
               // this.Salt = dt.Rows[0]["guest_salt"].ToString();
               // this.Password = dt.Rows[0]["guest_pwd"].ToString();
                this.Phone = dt.Rows[0]["guest_phone"].ToString();
            }
        }

        #endregion

        #region Methodes/Functions
        private static DataTable GetGuestByID(int id)
        {
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SE256_HoldenConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("guests_getbyid", cn);
            DataTable dt = new DataTable();

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@guests_id", SqlDbType.VarChar).Value = id;


            try
            {
                cn.Open();

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);
            }
            catch (Exception exc)
            {
                exc.ToString();
            }
            finally
            {
                cn.Close();
            }
            return dt;
        }
        public static bool InsertGuest(Guest gr)
        {
            bool blnSuccess = false;
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SE256_HoldenConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("guest_insert", cn);
            cmd.CommandType = CommandType.StoredProcedure;

            //cmd.Parameters.Add("", SqlDbType.).Value = gr.;
            cmd.Parameters.Add("guest_email", SqlDbType.VarChar).Value = gr.Email;
            cmd.Parameters.Add("guest_first", SqlDbType.VarChar).Value = gr.First;
            cmd.Parameters.Add("guest_last", SqlDbType.VarChar).Value = gr.Last;
            //cmd.Parameters.Add("guest_salt", SqlDbType.VarChar).Value = gr.Salt;
            //cmd.Parameters.Add("guest_pwd", SqlDbType.VarChar).Value = gr.Password;
            cmd.Parameters.Add("guest_phone", SqlDbType.VarChar).Value = gr.Phone;

            try
            {
                cn.Open();
                cmd.ExecuteNonQuery();
                blnSuccess = true;
            }
            catch (Exception exc)
            {
                exc.ToString();
                blnSuccess = false;
            }
            finally
            {
                cn.Close();
            }
            return blnSuccess;

        }

        public static bool UpdateGuest(Guest gr)
        {
            bool blnSuccess = false;
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SE256_HoldenConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("guest_update", cn);
            cmd.CommandType = CommandType.StoredProcedure;

            //cmd.Parameters.Add("", SqlDbType.).Value = gr.;
            cmd.Parameters.Add("guest_email", SqlDbType.VarChar).Value = gr.Email;
            cmd.Parameters.Add("guest_first", SqlDbType.VarChar).Value = gr.First;
            cmd.Parameters.Add("guest_last", SqlDbType.VarChar).Value = gr.Last;
            cmd.Parameters.Add("guest_salt", SqlDbType.VarChar).Value = gr.Salt;
            cmd.Parameters.Add("guest_pwd", SqlDbType.VarChar).Value = gr.Password;
            cmd.Parameters.Add("guest_phone", SqlDbType.VarChar).Value = gr.Phone;

            try
            {
                cn.Open();
                cmd.ExecuteNonQuery();
                blnSuccess = true;
            }
            catch (Exception exc)
            {
                exc.ToString();
                blnSuccess = false;
            }
            finally
            {
                cn.Close();
            }
            return blnSuccess;




        }

        private static DataTable GetGuestByEmail(string email)
        {
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SE256_HoldenConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("guests_getbyemail", cn);
            DataTable dt = new DataTable();

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@guests_email", SqlDbType.VarChar).Value = email;


            try
            {
                cn.Open();

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);
            }
            catch (Exception exc)
            {
                exc.ToString();
            }
            finally
            {
                cn.Close();
            }
            return dt;
        }
        #endregion
    }
}
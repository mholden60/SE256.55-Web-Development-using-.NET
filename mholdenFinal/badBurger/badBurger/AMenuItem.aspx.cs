﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace badBurger
{
    public partial class AMenuItem : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        { 
            if (!Request.IsAuthenticated)
            {
                Response.Redirect("~/Login.aspx");
            }
            int intID;

            if (RouteData.Values["item_id"] != null)
            {
            
                intID = Convert.ToInt32(RouteData.Values["item_id"].ToString());
            }
            else
            {
                // - or set it to a number that will never be a valid value (good for conditional population of the data on the page,
                intID = -1;
            }
            //- for example if id <> -1 fill customer record or if id = -1, prepare form for add record functionality
            if (!IsPostBack)
            {
                //bind form data objects procedure call, made on first visit to page
                BindData(intID);
            }

        }
       
        private void BindData(int id)
        {
            if (id != -1)
            {
                btnUpdate.Text = "Update";
                App_Code.MenuItem_Holden mi = new App_Code.MenuItem_Holden(id);
                if (mi != null)
                {
                    ddlMenu.SelectedValue = mi.MenuID;
                    ddlCategory.SelectedValue = mi.CatID;
                    txtName.Text = mi.ItemName;
                    txtDesc.Text = mi.Description;
                    txtAll.Text = mi.Allergens;
                    txtPrice.Text = mi.Price;
                    cbActive.Checked = mi.Active;
                }
            }
            else
            {
                btnUpdate.Text = "ADD";
            }
               
            
        }







        //Validate if checkbox is Checked
        protected void cvActive_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (cbActive.Checked)
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        protected void cbActive_CheckedChanged(object sender, EventArgs e)
        {

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            App_Code.MenuItem_Holden mi;
            if (RouteData.Values["item_id"] != null) 
            {
                mi = new App_Code.MenuItem_Holden(Convert.ToInt32(RouteData.Values["item_id"].ToString()));
            }
            else
            {
                mi = new App_Code.MenuItem_Holden();
            }
            mi.ItemName = txtName.Text.Trim();
            mi.Description = txtDesc.Text.Trim();
            mi.CatID = ddlCategory.SelectedValue.ToString();
            mi.MenuID = ddlMenu.SelectedValue.ToString();
            mi.Allergens = txtAll.Text.Trim();
            mi.Price = txtPrice.Text.Trim();
            mi.Active = cbActive.Checked;
            if(mi.ItemID > 0)
            {
                if(App_Code.MenuItem_Holden.UpdateMenuItem(mi))
                { 
                Response.Redirect("~/AMenuItems.aspx");
                }
                else
                {
                    lblActive.Text = "UPDATE FAILED";
                }
            }
            else
            {
                if(App_Code.MenuItem_Holden.InsertMenuItem(mi))
                {
                    Response.Redirect("~/AMenuItems");
                }
                else
                {
                    lblActive.Text = "INSERT FAILED";
                }
            }
            

        }

        protected void ddlMenu_PreRender(object sender, EventArgs e)
        {
            if(!IsPostBack)
            { 
            ddlMenu.Items.Insert(0, "Please Chose Menu Item");
            }
        }
    }
}
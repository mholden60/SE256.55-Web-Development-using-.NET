﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Collections;
using System.Data;
using System.Text;
using System.Resources;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Sql;


namespace badBurger
{
    public partial class Reservation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                Response.Redirect("~/Login.aspx");
            }
            int intID;
            //user the request namespace to determine a query string value
            if (RouteData.Values["res_id"] != null)
            {
                //- request.QueryString gets items from the query string
                //- convert the query string to the proper data type
                intID = Convert.ToInt32(RouteData.Values["res_id"].ToString());
            }
            else
            {
                // - or set it to a number that will never be a valid value (good for conditional population of the data on the page,
                intID = -1;
            }
            //- for example if id <> -1 fill customer record or if id = -1, prepare form for add record functionality

            if (!IsPostBack)
            {
                //bind form data objects procedure call, made on first visit to page
                BindData(intID);
            }
        }
        private void BindData(int intID)
        {
            if(intID != -1)
            {
                
            }
        }
        private DateTime ResDate(DateTime dtTime)
        {
            
             dtTime = Convert.ToDateTime(ddlHour.Text + ddlMin.Text + ddlAmPm.Text);
            return dtTime;
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            App_Code.Guest gus;
                if (RouteData.Values["guest_id"] != null)
            { 
                gus = new App_Code.Guest(Convert.ToInt32(RouteData.Values["guest_id"].ToString()));
            }
                else
            {
                gus = new App_Code.Guest();
            }
            gus.Email = txtEmail.Text;
            gus.First = txtFname.Text;
            gus.Last = txtLname.Text;
            gus.Phone = txtPhone.Text;

            if(gus.ID<0)
            {
                if(App_Code.Guest.UpdateGuest(gus))
                {
                    Response.Redirect("~/Guest.aspx");
                }
                
            }
            else
            {
                if(App_Code.Guest.InsertGuest(gus))
                {
                    Response.Redirect("~/Guest.aspx");
                }
            }
            //
            //App_Code.Guest guest;
            //App_Code.Reservation res;
            //
            //if (RouteData.Values["guest_email"] != null)
            //{
            //    guest = new App_Code.Guest(Convert.ToInt32(RouteData.Values["guest_email"].ToString()));
            //
            //}
            //else
            //{
            //    guest = new App_Code.Guest();
            //}
            //
            //guest.First = txtFname.Text.Trim();
            //guest.Last = txtLname.Text.Trim();
            //guest.Email = txtEmail.Text.Trim();
            //guest.Phone = txtPhone.Text.Trim();
            //
            // 
            //
            //if(guest.Email != null)
            //{
            //    if(App_Code.Guest.UpdateGuest(guest))
            //    {
            //        Response.Redirect("~/Guest.aspx");
            //    }
            //    else
            //    {
            //        
            //    }
            //}
            //else
            //{
            //    if(App_Code.Guest.InsertGuest(guest))
            //    {
            //        Response.Redirect("~/Guest.aspx");
            //    }
            //    else
            //    {
            //
            //    }
            //}

        }
    }
}
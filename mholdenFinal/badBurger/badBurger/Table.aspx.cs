﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace badBurger
{
    public partial class Table : System.Web.UI.Page
    {
        private int intID;

        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                Response.Redirect("~/Login.aspx");
            }
            int intID;

            if (RouteData.Values["tbl_id"] != null)
            {

                intID = Convert.ToInt32(RouteData.Values["tbl_id"].ToString());
            }
            else
            {
                // - or set it to a number that will never be a valid value (good for conditional population of the data on the page,
                intID = -1;
            }
            //- for example if id <> -1 fill customer record or if id = -1, prepare form for add record functionality
            if (!IsPostBack)
            {
                //bind form data objects procedure call, made on first visit to page
                BindData(intID);
            }
        }
        private void BindData(int intID)
        {
            if(intID != -1)
            {
                btnUpdate.Text = "Update";
                App_Code.TableClass tbl = new App_Code.TableClass(intID);
                if(tbl != null)
                {
                    txtName.Text = tbl.TblName;
                    txtDesc.Text = tbl.Tbldesc;
                    txtCount.Text = tbl.TblSeatCnt;
                    cbActive.Checked = tbl.Active;
                    ddlSection.SelectedValue = tbl.SectID.ToString();
                }
            }
            else
            {
                btnUpdate.Text = "Add";
            }
        }


        //Validate if checkbox is Checked
        protected void cvActive_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (cbActive.Checked)
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            App_Code.TableClass tbl;
            if(RouteData.Values["tbl_id"] != null)
            {
                tbl = new App_Code.TableClass(Convert.ToInt32(RouteData.Values["tbl_id"].ToString()));
            }
            else
            {
                tbl = new App_Code.TableClass();
            }
            tbl.TblName = txtName.Text.Trim();
            tbl.Tbldesc = txtDesc.Text.Trim();
            tbl.SectID = Convert.ToInt32(ddlSection.SelectedValue);
            tbl.TblSeatCnt = txtCount.Text.Trim();
            if( tbl.TblID > 0)
            {
                if(App_Code.TableClass.UpdateTable(tbl))
                {
                    Response.Redirect("~/Tables");
                }
                else
                {
                    lblActive.Text = "UPDATE FAIL";
                }
            }
            else
            {
                if(App_Code.TableClass.InsertTable(tbl))
                {
                    Response.Redirect("~/Tables");

                }
                else
                {
                    lblActive.Text = "Insert FAIL";
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Tables.aspx");
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BadBurger.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="badBurger.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- Content to be Displayed on screen to ensure the use of page is working --%>
    <h1>Login</h1>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <asp:Label runat="server" Text="UserName:"></asp:Label>
                    <asp:TextBox runat="server" ID="txtUname" CssClass="form-control"
                        placeholder="Username"></asp:TextBox>

                </div>
                <div class="form-group">
                    <asp:Label runat="server" Text="Password:"></asp:Label>
                    <asp:TextBox runat="server" ID="txtPword" CssClass="form-control" TextMode="Password"
                        placeholder="Password"></asp:TextBox>

                </div>
                <div class="form-group">
                    <asp:Button ID="btnLogin" runat="server" Text="Login" CausesValidation="true" OnClick="btnLogin_Click"/>
                    <asp:Button ID="btnForgot" runat="server" Text="Forgot Password" CausesValidation="true" />
                </div>
                <div>

                    <asp:Label ID="lblMessage" runat="server"></asp:Label>

                </div>
                <asp:ValidationSummary ID="validSummary" runat="server" DisplayMode="List" BorderStyle="Solid" />
                <asp:RequiredFieldValidator ID="RFVuser" runat="server" ErrorMessage="Username Required" ControlToValidate="txtUname" Display="None"></asp:RequiredFieldValidator><br />
                <asp:RegularExpressionValidator ID="validEmail" runat="server" ErrorMessage="Invaild Email" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$" ControlToValidate="txtUname" Display="None"></asp:RegularExpressionValidator><br />
                <asp:RequiredFieldValidator ID="RFVpassword" runat="server" ErrorMessage="Password Required" ControlToValidate="txtPword" Display="None"></asp:RequiredFieldValidator>

            </div>
        </div>
    </div>

</asp:Content>

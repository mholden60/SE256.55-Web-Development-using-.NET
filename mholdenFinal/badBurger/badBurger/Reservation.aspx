﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BadBurger.Master" AutoEventWireup="true" CodeBehind="Reservation.aspx.cs" Inherits="badBurger.Reservation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- Content to be Displayed on screen to ensure the use of page is working --%>
    THIS IS MY RESERVATION PAGE
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <asp:Calendar ID="cldCalendar" runat="server" SelectionMode="Day"></asp:Calendar>
                </div>
                <div class="form-group">
                    <asp:DropDownList ID="ddlHour" runat="server" ForeColor="Black">
                        <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                        <asp:ListItem Value="4">4</asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                        <asp:ListItem Value="6">6</asp:ListItem>
                        <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                        <asp:ListItem Value="9">9</asp:ListItem>
                        <asp:ListItem Value="10">10</asp:ListItem>
                        <asp:ListItem Value="11">11</asp:ListItem>
                        <asp:ListItem Value="12">12</asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddlMin" runat="server" ForeColor="Black">
                        <asp:ListItem Value="00">00</asp:ListItem>
                        <asp:ListItem Value="15">15</asp:ListItem>
                        <asp:ListItem Value="30">30</asp:ListItem>
                        <asp:ListItem Value="45">45</asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddlAmPm" runat="server" ForeColor="Black">
                        <asp:ListItem Value="AM">AM</asp:ListItem>
                        <asp:ListItem Value="PM">PM</asp:ListItem>
                    </asp:DropDownList>
                    <div class="form-group">
                        <asp:Label ID="lblFname" runat="server" Text="First Name: "></asp:Label>
                        <asp:TextBox ID="txtFname" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblLname" runat="server" Text="Last Name: "></asp:Label>
                        <asp:TextBox ID="txtLname" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblEmail" runat="server" Text="Email: "></asp:Label>
                        <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblPhone" runat="server" Text="Phone: "></asp:Label>
                        <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblGuestnum" runat="server" Text="Guest Number:"></asp:Label>

                        <asp:DropDownList ID="ddlGuestnum" runat="server" ForeColor="Black">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                            <asp:ListItem Value="3">3</asp:ListItem>
                            <asp:ListItem Value="4">4</asp:ListItem>
                            <asp:ListItem Value="5">5</asp:ListItem>
                            <asp:ListItem Value="6">6</asp:ListItem>
                            <asp:ListItem Value="7">7</asp:ListItem>
                            <asp:ListItem Value="8">8</asp:ListItem>
                            <asp:ListItem Value="9">9</asp:ListItem>
                            <asp:ListItem Value="10">10</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                    </div>
                    <asp:RequiredFieldValidator ID="rfvFname" runat="server" ErrorMessage="FIRST NAME REQUIRED!" ForeColor="Red" ControlToValidate="txtFname"></asp:RequiredFieldValidator><br />
                    <asp:RequiredFieldValidator ID="rfvLname" runat="server" ErrorMessage="LAST NAME REQUIRED!" ForeColor="Red" ControlToValidate="txtLname"></asp:RequiredFieldValidator><br />
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="EMAIL REQUIRED!" ForeColor="Red" ControlToValidate="txtEmail"></asp:RequiredFieldValidator><br />
                    <asp:RequiredFieldValidator ID="rfvPhone" runat="server" ErrorMessage="PHONE NUMBER REQUIRED!" ForeColor="Red" ControlToValidate="txtPhone"></asp:RequiredFieldValidator><br />
                </div>
            </div>
        </div>
    </div>

</asp:Content>

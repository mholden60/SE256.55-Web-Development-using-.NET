﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Collections;
using System.Data;
using System.Text;
using System.Resources;

namespace badBurger
{
    public partial class BadBurger : System.Web.UI.MasterPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Populate the Menu bar from MENUS
                List<MenuItem> menu = Menus.GetMainMenu();
                foreach (MenuItem mi in menu)
                {
                    muTest.Items.Add(mi);
                }
                if (!Request.IsAuthenticated || Session["Fullname"] == "")
                {
                    lblHome.Text = "Welcome Stranger!";
                    btnLogout.Visible = false;
                    btnLogout.Enabled = false;
                }
                else
                {
                    lblHome.Text = "Welcome " + Session["Fullname"];
                }
            }
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {

                FormsAuthentication.SignOut();
                Session["Fullname"] = null;
                Response.Redirect("~/Login.aspx");

                btnLogout.Visible = true;
                btnLogout.Enabled = true;

            }
            else
            {

                btnLogout.Visible = false;
                btnLogout.Enabled = false;

            }

        }
    }




































}
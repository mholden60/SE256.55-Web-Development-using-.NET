﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BadBurger.Master" AutoEventWireup="true" CodeBehind="ForgotPassword.aspx.cs" Inherits="badBurger.ForgotPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- Content to be Displayed on screen to ensure the use of page is working --%>
    THIS IS MY FORGOT PASSWORD PAGE
        <h1>Forgot Password</h1>
    <div class="container-fluid">
        <div class="row">

            <div class="col-sm-4">
                <%--************************************Forms*****************************************************************************************--%>
                <div class="form-group">
                    <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblConfirm" runat="server" Text="Confirm Email"></asp:Label>
                    <asp:TextBox ID="txtCmail" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblPassword" runat="server" Text="Password"></asp:Label>
                    <asp:TextBox ID="txtPword" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblCword" runat="server" Text="Confirm Password"></asp:Label>
                    <asp:TextBox ID="txtCword" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">

                    <asp:Button ID="btnReset" runat="server" Text="Reset Password" />
                </div>
            </div>
        </div>
    
    <%--*****************************************************************************************************************************--%>


    <%--*************************************Validation*****************************************************************************--%>
    <asp:ValidationSummary ID="summary" DisplayMode="BulletList" BorderStyle="Solid"  runat="server" />
    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="Email Required" Display="None" ControlToValidate="txtEmail"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="rfvConfirm" runat="server" ErrorMessage="Confirm Email Required" Display="None" ControlToValidate="txtCmail"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="rfvPword" runat="server" ErrorMessage="Password Required" Display="None" ControlToValidate="txtPword"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="rfvCword" runat="server" ErrorMessage="Confirm Password Required" Display="None" ControlToValidate="txtCword"></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator ID="validEmail" runat="server" ErrorMessage="Invaild Email" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$" ControlToValidate="txtEmail" Display="None"></asp:RegularExpressionValidator><br />
    <asp:RegularExpressionValidator ID="validCmail" runat="server" ErrorMessage="Invaild Confirm Email" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$" ControlToValidate="txtCmail" Display="None"></asp:RegularExpressionValidator><br />
    <asp:CompareValidator ID="compareEmail" runat="server" ErrorMessage="Email not Identical" Display="None" ControlToCompare="txtCmail" ControlToValidate="txtEmail"></asp:CompareValidator>
    <asp:CompareValidator ID="comparePassword" runat="server" ErrorMessage="Password not Identical" Display="None" ControlToCompare="txtCword" ControlToValidate="txtPword"></asp:CompareValidator>
    <%--*************************************Validation*****************************************************************************--%>
        </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Collections;
using System.Data;
using System.Text;
using System.Resources;

namespace badBurger
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(txtUname.Text) && !string.IsNullOrEmpty(txtPword.Text))
            {

                AppUser u = AppUser.Login(txtUname.Text.Trim(), txtPword.Text.Trim());

                if (u.ValidLogin)
                {
                    FormsAuthenticationTicket t =
                   new FormsAuthenticationTicket(
                       1,
                       u.UserId.ToString(),
                       DateTime.Now,
                       DateTime.Now.AddMinutes(480),
                       false,
                       "Admin");

                    //For security reasons we may hash the cookies
                    string encryptedTicked = FormsAuthentication.Encrypt(t);
                    HttpCookie c = new HttpCookie(
                        FormsAuthentication.FormsCookieName, encryptedTicked);
                    //add cookie to response
                    Response.Cookies.Add(c);

                    //set the username to a client side cookie for future reference
                    Session["FullName"] = string.Concat(
                        u.LastName,
                       ", ", u.FirstName);

                    Response.Redirect("~/User.aspx");

                }
                else
                {
                    lblMessage.Text = "Login Failed!";
                }


            }
        }
    }
}
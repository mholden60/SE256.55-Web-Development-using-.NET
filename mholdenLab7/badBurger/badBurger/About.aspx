﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BadBurger.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="badBurger.About" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- Content to be Displayed on screen to ensure the use of page is working --%>
    THIS IS MY ABOUT US PAGE
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
                <h1>ABOUT US</h1>
                   On the first day of summer, a slacking high school student, Dexter Reed (Kenan Thompson), takes his mother's car on a joyride while she is on a business trip and accidentally crashes into and damages the car of his teacher, Mr. Wheat. (Sinbad). Dexter is in danger of going to jail, as he does not have a driver's license or insurance. But Mr. Wheat agrees to let Dexter pay for the damages to both cars in exchange for not calling the police on Dexter. After discovering that the damages will total $2,500, Dexter is forced to get a summer job. He ends up finding employment at Bad Burger where he meets and reluctantly befriends dimwitted Ed (Kel Mitchell) and a host of other colorful employees. Initially, neither of them are aware that it was in fact Ed who inadvertently caused Dexter's car accident; Ed had been rollerblading to make a delivery and skated in front of Dexter at a crossroad, causing him to swerve and crash into Mr. Wheat's car
            </div>
            <div class="col-sm-4"></div>
        </div>
    </div>
</asp:Content>

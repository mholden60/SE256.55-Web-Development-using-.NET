﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="badBurger.Users" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- Content to be Displayed on screen to ensure the use of page is working --%>
    THIS IS MY USERS PAGE
    <div class="table-responsive">
        <div class="col-med-12">
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:SE256_HoldenConnectionString %>" SelectCommand="users_getall" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="user_id" DataSourceID="SqlDataSource1">
                <Columns>
                    <asp:HyperLinkField DataNavigateUrlFields="user_id" DataNavigateUrlFormatString="~/Admin/User/{0}" DataTextField="user_email" HeaderText="User Name" />                    
                    <asp:BoundField DataField="user_first" HeaderText="First Name" SortExpression="user_first" />
                    <asp:BoundField DataField="user_last" HeaderText="Last Name" SortExpression="user_last" />
                    <asp:BoundField DataField="user_add1" HeaderText="Address 1" SortExpression="user_add1" />
                    <asp:BoundField DataField="user_add2" HeaderText="Address 2" SortExpression="user_add2" />
                    <asp:BoundField DataField="user_city" HeaderText="City" SortExpression="user_city" />
                    <asp:BoundField DataField="state_id" HeaderText="State" SortExpression="state_id" />
                    <asp:BoundField DataField="user_zip" HeaderText="Zip" SortExpression="user_zip" />
                    <asp:BoundField DataField="user_phone" HeaderText="Phone" SortExpression="user_phone" />
                    <asp:CheckBoxField DataField="user_active" HeaderText="Active" SortExpression="user_active" />

                </Columns>
                <PagerSettings Position="Bottom" PageButtonCount="2" />
                <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
            </asp:GridView>
        </div>
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace badBurger
{
    public partial class Section : System.Web.UI.Page
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            //statndard check for authenticated user
            //check for authenticated user send to login page if not 
            //authenticated       
            if (!Request.IsAuthenticated)
            {
                Response.Redirect("~/Login.aspx");
            }
            int intID;
            //user the request namespace to determine a query string value
            if (RouteData.Values["sect_id"] != null)
            {
                //- request.QueryString gets items from the query string
                //- convert the query string to the proper data type
                intID = Convert.ToInt32(RouteData.Values["sect_id"].ToString());
            }
            else
            {
                // - or set it to a number that will never be a valid value (good for conditional population of the data on the page,
                intID = -1;
            }
            //- for example if id <> -1 fill customer record or if id = -1, prepare form for add record functionality

            if (!IsPostBack)
            {
                //bind form data objects procedure call, made on first visit to page
                BindData(intID);
            }
        }

        private void BindData(int intID)
        {
            if(intID != -1)
            {
                btnUpdate.Text = "Update";
                App_Code.Section sec = new App_Code.Section(intID);
                if(sec!=null)
                {
                    txtName.Text = sec.SectName;
                    txtDesc.Text = sec.Description;
                    cbActive.Checked = sec.Active;
                }

            }
            else
            {
                btnUpdate.Text = "Add";
            }
        }

        





        















        //Vaidate if Checkbox is checked
        protected void cvActive_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (cbActive.Checked)
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            App_Code.Section sec;
            if(RouteData.Values["sect_id"] != null)
            {
                sec = new App_Code.Section(Convert.ToInt32(RouteData.Values["sect_id"].ToString()));
            }
            else
            {
                sec = new App_Code.Section();
            }
            sec.SectName = txtName.Text.Trim();
            sec.Description = txtDesc.Text.Trim();
            sec.Active = Convert.ToBoolean(cbActive.Checked.ToString());
            if (sec.SectID > 0)
            {
                if(App_Code.Section.UpdateSection(sec))
                {
                    Response.Redirect("~/Sections");
                }
                else
                {
                    lblDesc.Text = "UPDATE FAIL";
                }
            }
            else
            {
                if( App_Code.Section.InsertSection(sec))
                {
                    Response.Redirect("~/Sections");
                }
                else
                {
                    lblDesc.Text = "UPDATE FAIL";
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Sections.aspx");
        }
    }
}
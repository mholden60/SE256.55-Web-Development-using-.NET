﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace badBurger.App_Code
{ 
    public class MenuItem_Holden
    {
        #region Properties
        public int ItemID { get; set; }
        public string MenuID { get; set; }
        public string CatID { get; set; }
        public string ItemName { get; set; }
        public string Description { get; set; }
        public string Allergens { get; set; }
        public string Price { get; set; }
        public Boolean Gluten { get; set; }
        public Boolean Active { get; set; }
        #endregion

        #region Constructors
        public MenuItem_Holden()
        { }
        public MenuItem_Holden(int id)
        {
            DataTable dt = new DataTable();
            dt = GetMenuItemByID(id);
            if(dt.Rows.Count > 0)
            {
                this.ItemID = Convert.ToInt32(dt.Rows[0]["item_id"].ToString());
                this.ItemName = dt.Rows[0]["item_name"].ToString();
                this.Description = dt.Rows[0]["item_desc"].ToString();
                this.Allergens = dt.Rows[0]["item_allergens"].ToString();
                this.Price = dt.Rows[0]["item_price"].ToString();
                this.Gluten = Convert.ToBoolean(dt.Rows[0]["item_gluten_free"].ToString());
                this.Active = Convert.ToBoolean(dt.Rows[0]["item_active"].ToString());

                
            }
        }
        #endregion
        #region Methodes/Functions
        private static DataTable GetMenuItemByID(int id)
        {
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SE256_HoldenConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("menu_items_getbyid", cn);
            DataTable dt = new DataTable();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("item_id", SqlDbType.VarChar).Value = id;
            try
            {
                cn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch(Exception exc)
            {
                exc.ToString();
            }
            finally
            {
                cn.Close();
            }
            return dt;
        }
        public static bool InsertMenuItem (MenuItem_Holden mi)
        {
            bool blnSuccess = false;

            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SE256_HoldenConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("menu_items_insert", cn);
            cmd.CommandType = CommandType.StoredProcedure;

            // cmd.Parameters.Add("item_id", SqlDbType.Int).Value = mi.ItemID;
            cmd.Parameters.Add("menu_id", SqlDbType.Int).Value = mi.MenuID;
            cmd.Parameters.Add("cat_id", SqlDbType.Int).Value = mi.CatID;
            cmd.Parameters.Add("item_name", SqlDbType.VarChar).Value = mi.ItemName;
            cmd.Parameters.Add("item_desc", SqlDbType.VarChar).Value = mi.Description;
            cmd.Parameters.Add("item_allergens", SqlDbType.VarChar).Value = mi.Allergens;
            cmd.Parameters.Add("item_price", SqlDbType.VarChar).Value = mi.Price;
            cmd.Parameters.Add("item_gluten_free", SqlDbType.Bit).Value = mi.Gluten;
            cmd.Parameters.Add("item_active", SqlDbType.Bit).Value = mi.Active;

            try
            {
                cn.Open();
                cmd.ExecuteNonQuery();
                blnSuccess = true;
            }
            catch (Exception exc)
            {
                exc.ToString();
                blnSuccess = false;
            }
            finally
            {
                cn.Close();
            }
            return blnSuccess;

        }
        public static bool UpdateMenuItem(MenuItem_Holden mi)
        {
            bool blnSuccess = false;

            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SE256_HoldenConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("menu_items_update", cn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("item_id", SqlDbType.Int).Value = mi.ItemID;

            cmd.Parameters.Add("menu_id", SqlDbType.Int).Value = mi.MenuID;
            cmd.Parameters.Add("cat_id", SqlDbType.Int).Value = mi.CatID;
            cmd.Parameters.Add("item_name", SqlDbType.VarChar).Value = mi.ItemName;
            cmd.Parameters.Add("item_desc", SqlDbType.VarChar).Value = mi.Description;
            cmd.Parameters.Add("item_allergens", SqlDbType.VarChar).Value = mi.Allergens;
            cmd.Parameters.Add("item_price", SqlDbType.VarChar).Value = mi.Price;
            cmd.Parameters.Add("item_gluten_free", SqlDbType.Bit).Value = mi.Gluten;
            cmd.Parameters.Add("item_active", SqlDbType.Bit).Value = mi.Active;

            try
            {
                cn.Open();
                cmd.ExecuteNonQuery();
                blnSuccess = true;
            }
            catch (Exception exc)
            {
                exc.ToString();
                blnSuccess = false;
            }
            finally
            {
                cn.Close();
            }
            return blnSuccess;

        }
        #endregion
    }
}
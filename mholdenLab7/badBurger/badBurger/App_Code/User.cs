﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace badBurger.App_Code
{
    public class User
    {
        #region Properties
        public int UserID { get; set; }
        public string Email { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string Add1 { get; set; }
        public string Add2 { get; set; }
        public string City { get; set; }
        public string Ccity { get; set; }
        public string StateID { get; set; }
        public string Zip { get; set; }
        public string Salt { get; set; }
        public string PWD { get; set; }
        public string Cpwd { get; set; }
        public string Phone { get; set; }
        public Boolean Active { get; set; }
        #endregion
        #region Constructor
        public User()
        {

        }
        public User(int intID)
        {
            DataTable dt = new DataTable();
            dt = GetUserByID(intID);
            if(dt.Rows.Count > 0)
            {
                this.UserID = Convert.ToInt32(dt.Rows[0]["user_id"].ToString());
                this.Email = dt.Rows[0]["user_email"].ToString();
                this.FName = dt.Rows[0]["user_first"].ToString();
                this.LName = dt.Rows[0]["user_last"].ToString();
                this.Add1 = dt.Rows[0]["user_add1"].ToString();
                this.Add2 = dt.Rows[0]["user_add2"].ToString();
                this.City = dt.Rows[0]["user_city"].ToString();
                this.StateID = dt.Rows[0]["state_id"].ToString();
                this.Zip = dt.Rows[0]["user_zip"].ToString();
                this.Salt = dt.Rows[0]["user_salt"].ToString();
                this.PWD = dt.Rows[0]["user_pwd"].ToString();
                this.Phone = dt.Rows[0]["user_phone"].ToString();
                this.Active = Convert.ToBoolean(dt.Rows[0]["user_active"].ToString());
                
            }
        }
        #endregion
        #region Methodes/Functions
        private static DataTable GetUserByID(int intID)
        {
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SE256_HoldenConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("users_getbyid", cn);
            DataTable dt = new DataTable();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("user_id", SqlDbType.VarChar).Value = intID;

            try
            {
                cn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception exc)
            {
                exc.ToString();
            }
            finally
            {
                cn.Close();
            }
            return dt;
        }
        public static bool InsertUser( User use)
        {
            bool blnSuccess = false;
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SE256_HoldenConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("users_insert", cn);
            DataTable dt = new DataTable();
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("user_email", SqlDbType.VarChar).Value = use.Email;
            cmd.Parameters.Add("user_first", SqlDbType.VarChar).Value = use.FName;
            cmd.Parameters.Add("user_last", SqlDbType.VarChar).Value = use.LName;
            cmd.Parameters.Add("user_add1", SqlDbType.VarChar).Value = use.Add1;
            cmd.Parameters.Add("user_add2", SqlDbType.VarChar).Value = use.Add2;
            cmd.Parameters.Add("user_city", SqlDbType.VarChar).Value = use.City;
            cmd.Parameters.Add("state_id", SqlDbType.VarChar).Value = use.StateID;
            cmd.Parameters.Add("user_zip", SqlDbType.VarChar).Value = use.Zip;
            cmd.Parameters.Add("user_salt", SqlDbType.VarChar).Value = use.Salt;
            cmd.Parameters.Add("user_pwd", SqlDbType.VarChar).Value = use.PWD;
            cmd.Parameters.Add("user_phone", SqlDbType.VarChar).Value = use.Phone;
            cmd.Parameters.Add("user_active", SqlDbType.VarChar).Value = use.Active;

            try
            {
                cn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception exc)
            {
                exc.ToString();
            }
            finally
            {
                cn.Close();
            }
            return blnSuccess;
            
        }
        public static bool UpdateUser(User use)
        {
            bool blnSuccess = false;
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SE256_HoldenConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("users_update", cn);
            DataTable dt = new DataTable();
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("user_id", SqlDbType.VarChar).Value = use.UserID;
            cmd.Parameters.Add("user_email", SqlDbType.VarChar).Value = use.Email;
            cmd.Parameters.Add("user_first", SqlDbType.VarChar).Value = use.FName;
            cmd.Parameters.Add("user_last", SqlDbType.VarChar).Value = use.LName;
            cmd.Parameters.Add("user_add1", SqlDbType.VarChar).Value = use.Add1;
            cmd.Parameters.Add("user_add2", SqlDbType.VarChar).Value = use.Add2;
            cmd.Parameters.Add("user_city", SqlDbType.VarChar).Value = use.City;
            cmd.Parameters.Add("state_id", SqlDbType.VarChar).Value = use.StateID;
            cmd.Parameters.Add("user_zip", SqlDbType.VarChar).Value = use.Zip;
            cmd.Parameters.Add("user_salt", SqlDbType.VarChar).Value = use.Salt;
            cmd.Parameters.Add("user_pwd", SqlDbType.VarChar).Value = use.PWD;
            cmd.Parameters.Add("user_phone", SqlDbType.VarChar).Value = use.Phone;
            cmd.Parameters.Add("user_active", SqlDbType.VarChar).Value = use.Active;

            try
            {
                cn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception exc)
            {
                exc.ToString();
            }
            finally
            {
                cn.Close();
            }
            return blnSuccess;

        }
        #endregion
    }
}
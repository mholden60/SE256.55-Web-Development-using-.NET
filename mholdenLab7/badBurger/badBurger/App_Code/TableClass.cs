﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace badBurger.App_Code
{
    public class TableClass
    {
        #region Properties
        public int TblID { get; set; }
        public int SectID { get; set; }
        public string TblName { get; set; }
        public string Tbldesc { get; set; }
        public string TblSeatCnt { get; set; }
        public bool Active { get; set; }
        #endregion


        #region Constructors
        public TableClass()
        { }
        public TableClass(int intID)
        {
            DataTable dt = new DataTable();
            dt = GetTableByID(intID);
            if(dt.Rows.Count > 0)
            {
                this.TblID = Convert.ToInt32(dt.Rows[0]["tbl_id"].ToString());
                this.SectID = Convert.ToInt32(dt.Rows[0]["sect_id"].ToString());
                this.TblName = dt.Rows[0]["tbl_name"].ToString();
                this.Tbldesc = dt.Rows[0]["tbl_desc"].ToString();
                this.TblSeatCnt = dt.Rows[0]["tbl_seat_cnt"].ToString();
                this.Active = Convert.ToBoolean(dt.Rows[0]["tbl_active"].ToString());

            }
        }
        #endregion


        #region Methods/Function
        private static DataTable GetTableByID(int intID)
        {
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SE256_HoldenConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("tables_getbyid", cn);
            DataTable dt = new DataTable();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("tbl_id", SqlDbType.VarChar).Value = intID;

            try
            {
                cn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception exc)
            {
                exc.ToString();
            }
            finally
            {
                cn.Close();
            }
            return dt;
        }
        public static bool InsertTable(TableClass tbl)
        {
            bool blnSuccess = false;
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SE256_HoldenConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("tables_insert", cn);
            DataTable dt = new DataTable();
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("sect_id", SqlDbType.Int).Value = tbl.SectID;
            cmd.Parameters.Add("tbl_name", SqlDbType.VarChar).Value = tbl.TblName;
            cmd.Parameters.Add("tbl_desc", SqlDbType.VarChar).Value = tbl.Tbldesc;
            cmd.Parameters.Add("tbl_seat_cnt", SqlDbType.VarChar).Value = tbl.TblSeatCnt;
            cmd.Parameters.Add("tbl_active", SqlDbType.VarChar).Value = tbl.Active;
            try
            {
                cn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception exc)
            {
                exc.ToString();
            }
            finally
            {
                cn.Close();
            }
            return blnSuccess;
        }
        public static bool UpdateTable(TableClass tbl)
        {
            bool blnSuccess = false;
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SE256_HoldenConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("tables_insert", cn);
            DataTable dt = new DataTable();
            cmd.CommandType = CommandType.StoredProcedure;

            //cmd.Parameters.Add("tbl_id", SqlDbType.Int).Value = tbl.TblID;
            cmd.Parameters.Add("sect_id", SqlDbType.Int).Value = tbl.SectID;
            cmd.Parameters.Add("tbl_name", SqlDbType.VarChar).Value = tbl.TblName;
            cmd.Parameters.Add("tbl_desc", SqlDbType.VarChar).Value = tbl.Tbldesc;
            cmd.Parameters.Add("tbl_seat_cnt", SqlDbType.VarChar).Value = tbl.TblSeatCnt;
            cmd.Parameters.Add("tbl_active", SqlDbType.VarChar).Value = tbl.Active;
            try
            {
                cn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception exc)
            {
                exc.ToString();
            }
            finally
            {
                cn.Close();
            }
            return blnSuccess;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace badBurger.App_Code
{
    public class Reservation
    {
        #region Properties
        public int ResID { get; set; }
        public int GuestID { get; set; }
        public int TblID { get; set; }
        public int UserID { get; set; }
        public DateTime ResDate { get; set; }
        public DateTime ResTime { get; set; }
        public int Count { get; set; }
        public string Request { get; set; }
        #endregion

        #region Constructors
        public Reservation()
        {

        }

        public Reservation(int id)
        {
            DataTable dt = new DataTable();
            dt = GetResByID(id);
            if (dt.Rows.Count > 0)
            {
                this.ResID = Convert.ToInt32(dt.Rows[0]["res_id"].ToString());
                this.GuestID = Convert.ToInt32(dt.Rows[0]["guest_id"].ToString());
                this.TblID = Convert.ToInt32(dt.Rows[0]["tbl_id"].ToString());
                this.UserID = Convert.ToInt32(dt.Rows[0]["user_id"].ToString());
                this.ResDate = Convert.ToDateTime(dt.Rows[0]["res_date"].ToString());
                this.ResTime = Convert.ToDateTime(dt.Rows[0]["res_time"].ToString());
                this.Count = Convert.ToInt32(dt.Rows[0]["res_guest_cnt"].ToString());
                this.Request = dt.Rows[0]["res_spec_req"].ToString();
            }
        }


        #endregion
        #region Methodes/Functions
        private static DataTable GetResByID(int intID)
        {

            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SE256_HoldenConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("reservations_getbyid", cn);
            DataTable dt = new DataTable();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("res_id", SqlDbType.VarChar).Value = intID;

            try
            {
                cn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception exc)
            {
                exc.ToString();
            }
            finally
            {
                cn.Close();
            }
            return dt;

        }
        public static bool InsertReservation(Reservation res)
        {
            bool blnSuccess = false;
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SE256_HoldenConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("reservation_insert", cn);
            DataTable dt = new DataTable();
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("guest_id", SqlDbType.Int).Value = res.GuestID;
            cmd.Parameters.Add("tbl_id", SqlDbType.Int).Value = res.TblID;
            cmd.Parameters.Add("user_id", SqlDbType.Int).Value = res.UserID;
            cmd.Parameters.Add("res_date", SqlDbType.Date).Value = res.ResDate;
            cmd.Parameters.Add("res_time", SqlDbType.Time).Value = res.ResTime;
            cmd.Parameters.Add("res_guest_cnt", SqlDbType.Int).Value = res.Count;
            cmd.Parameters.Add("res_spec_req", SqlDbType.VarChar).Value = res.Request;

            try
            {
                cn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception exc)
            {
                exc.ToString();
            }
            finally
            {
                cn.Close();
            }
            return blnSuccess;
        }

        public static bool UpdateReservation(Reservation res)
        {
            bool blnSuccess = false;
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SE256_HoldenConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("reservation_insert", cn);
            DataTable dt = new DataTable();
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("res_id", SqlDbType.Int).Value = res.ResID;
            cmd.Parameters.Add("guest_id", SqlDbType.Int).Value = res.GuestID;
            cmd.Parameters.Add("tbl_id", SqlDbType.Int).Value = res.TblID;
            cmd.Parameters.Add("user_id", SqlDbType.Int).Value = res.UserID;
            cmd.Parameters.Add("res_date", SqlDbType.Date).Value = res.ResDate;
            cmd.Parameters.Add("res_time", SqlDbType.Time).Value = res.ResTime;
            cmd.Parameters.Add("res_guest_cnt", SqlDbType.Int).Value = res.Count;
            cmd.Parameters.Add("res_spec_req", SqlDbType.VarChar).Value = res.Request;

            try
            {
                cn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception exc)
            {
                exc.ToString();
            }
            finally
            {
                cn.Close();
            }
            return blnSuccess;
        }

    }

    #endregion
}

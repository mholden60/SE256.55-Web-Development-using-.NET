﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace badBurger
{
    public partial class Reservation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                Response.Redirect("~/Login.aspx");
            }
            int intID;
            //user the request namespace to determine a query string value
            if (RouteData.Values["res_id"] != null)
            {
                //- request.QueryString gets items from the query string
                //- convert the query string to the proper data type
                intID = Convert.ToInt32(RouteData.Values["res_id"].ToString());
            }
            else
            {
                // - or set it to a number that will never be a valid value (good for conditional population of the data on the page,
                intID = -1;
            }
            //- for example if id <> -1 fill customer record or if id = -1, prepare form for add record functionality

            if (!IsPostBack)
            {
                //bind form data objects procedure call, made on first visit to page
                BindData(intID);
            }
        }
        private void BindData(int intID)
        {
            if(intID != -1)
            {
                
            }
        }

    }
}
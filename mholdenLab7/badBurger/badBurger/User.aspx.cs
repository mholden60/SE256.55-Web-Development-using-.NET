﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace badBurger
{
    public partial class User : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                Response.Redirect("~/Login.aspx");
            }
            int intID;
            //user the request namespace to determine a query string value
            if (RouteData.Values["user_id"] != null)
            {
                //- request.QueryString gets items from the query string
                //- convert the query string to the proper data type
                intID = Convert.ToInt32(RouteData.Values["user_id"].ToString());
            }
            else
            {
                // - or set it to a number that will never be a valid value (good for conditional population of the data on the page,
                intID = -1;
            }
            //- for example if id <> -1 fill customer record or if id = -1, prepare form for add record functionality

            if (!IsPostBack)
            {
                //bind form data objects procedure call, made on first visit to page
                BindData(intID);
            }
        }
        private void BindData(int intID)
        {
            if(intID != -1)
            {
                btnUpdate.Text = "Update";
                App_Code.User use = new App_Code.User(intID);
                if (use != null)
                {
                    //txtCemail.Text = use.Email;
                    txtFname.Text = use.FName;
                    txtLname.Text = use.LName;
                    txtAddress.Text = use.Add1;
                    txtAddress2.Text = use.Add2;
                    txtCity.Text = use.City;
                    ddlStates.Text = use.StateID;
                    txtZip.Text = use.Zip;
                    txtEmail.Text = use.Email;
                    //txtPword.Text = use.PWD;
                    //txtCword.Text = use.Cpwd;
                    txtPhone.Text = use.Phone;
                    cbActive.Checked = use.Active;

                }
                else
                {
                    btnUpdate.Text = "ADD";
                }
            }
        }

        protected void cvActive_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (cbActive.Checked)
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            App_Code.User use;
            if(RouteData.Values["user_id"] != null)
            {
                use = new App_Code.User(Convert.ToInt32(RouteData.Values["user_id"].ToString()));

            }
            else
            {
                use = new App_Code.User();
            }
            use.FName = txtFname.Text.Trim();
            use.LName = txtLname.Text.Trim();
            use.Add1 = txtAddress.Text.Trim();
            use.Add2 = txtAddress2.Text.Trim();
            use.City = txtCity.Text.Trim();
            use.StateID = ddlStates.SelectedValue.ToString();
            use.Zip = txtZip.Text.Trim();
            //use.PWD = txtPword.Text.Trim();
            if(use.UserID > 0)
            {
                if(App_Code.User.UpdateUser(use))
                {
                    Response.Redirect("~/User");

                }
                else
                {
                    lblActive.Text = "UPDATE FAIL";
                }
            }
            else
            {
                if(App_Code.User.InsertUser(use))
                {
                    Response.Redirect("~/User");
                }
                else
                {
                    lblActive.Text = "INSERT FAIL";
                }
            }
            
        }
    }
}
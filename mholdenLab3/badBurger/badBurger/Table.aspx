﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Table.aspx.cs" Inherits="badBurger.Table" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- Content to be Displayed on screen to ensure the use of page is working --%>

    <h1>Table</h1>
    <div class="container-fluid">
        <div class="row">

            <div class="col-sm-4">
                <%--************************************Forms*****************************************************************************************--%>
                <div class="form-group">
                    <asp:Label ID="lblName" runat="server" Text="Name"></asp:Label>
                    <asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblDesc" runat="server" Text="Describe"></asp:Label>
                    <asp:TextBox ID="txtDesc" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <asp:Label ID="lblSection" runat="server" Text="Section"></asp:Label>
                <asp:DropDownList ID="ddlSection" runat="server" CssClass="form-group" BackColor="Black">
                    <asp:ListItem>Please Choose One</asp:ListItem>
                    <asp:ListItem>Test Data 1</asp:ListItem>
                    <asp:ListItem>Test Data 2</asp:ListItem>
                    <asp:ListItem>Test Data 3</asp:ListItem>
                </asp:DropDownList>
                <div class="form-group">
                    <asp:Label ID="lblSeat" runat="server" Text="Seat Count"></asp:Label>
                    <asp:TextBox ID="txtCount" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblActive" runat="server" Text="Is Active"></asp:Label>
                    <asp:CheckBox ID="cbActive" runat="server" CausesValidation="True" />
                </div>

                <div class="form-group">
                    <asp:Button ID="btnUpdate" runat="server" Text="Update" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false" PostBackUrl="~/Default.aspx" />
                </div>
                <%--***********************************VALIDATION***********************************************************************************************************--%>
                <div>
                    <asp:ValidationSummary ID="validSummary" runat="server" BorderStyle="Solid" />
                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="Name is Required" ControlToValidate="txtName" Display="None"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rfvDesc" runat="server" ErrorMessage="Description is Required" ControlToValidate="txtDesc" Display="None"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="cvActive" runat="server" ErrorMessage="Please check Active Box" OnServerValidate="cvActive_ServerValidate" Display="None"></asp:CustomValidator>
                    <asp:RequiredFieldValidator ID="rfvSeat" runat="server" ErrorMessage="Seat Count is Required" ControlToValidate="txtCount" Display="None"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rfvSection" runat="server" ErrorMessage="Please Select a Section" InitialValue="Please Choose One" Display="None" ControlToValidate="ddlSection"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="cvAvtive" runat="server" ErrorMessage="Please check Active Box" OnServerValidate="cvActive_ServerValidate" Display="None"></asp:CustomValidator>

                </div>
            </div>
        </div>
    </div>
</asp:Content>

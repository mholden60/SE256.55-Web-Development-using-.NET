﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AMenuItem.aspx.cs" Inherits="badBurger.AMenuItem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- Content to be Displayed on screen to ensure the use of page is working --%>
    <h1>Menu Item</h1>
    <div class="container-fluid">
        <div class="row">

            <div class="col-sm-4">
                <%--************************************Forms*****************************************************************************************--%>
                <div class="form-group">
                    <asp:Label ID="lblName" runat="server" Text="Name"></asp:Label>
                    <asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblDesc" runat="server" Text="Description"></asp:Label>
                    <asp:TextBox ID="txtDesc" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblAll" runat="server" Text="Allergens"></asp:Label>
                    <asp:TextBox ID="txtAll" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblPrice" runat="server" Text="Price"></asp:Label>
                    <asp:TextBox ID="txtPrice" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblMenu" runat="server" Text="Menu"></asp:Label>
                    <asp:DropDownList ID="ddlMenu" runat="server" BackColor="Black">
                        <asp:ListItem>Please Choose One</asp:ListItem>
                        <asp:ListItem>Breakfast</asp:ListItem>
                        <asp:ListItem>Lunch</asp:ListItem>
                        <asp:ListItem>Dinner</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblCategory" runat="server" Text="Category"></asp:Label>
                    <asp:DropDownList ID="ddlCategory" runat="server" BackColor="Black">
                        <asp:ListItem>Please Choose One</asp:ListItem>
                        <asp:ListItem>Appetizers</asp:ListItem>
                        <asp:ListItem>Entrees</asp:ListItem>
                        <asp:ListItem>Desserts</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblActive" runat="server" Text="Is Active"></asp:Label>
                    <asp:CheckBox ID="cbActive" runat="server" CausesValidation="True" OnCheckedChanged="cbActive_CheckedChanged" />
                </div>
                <div class="form-group">
                    <asp:Button ID="btnUpdate" runat="server" Text="Update" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false" PostBackUrl="~/Default.aspx" />
                </div>
                <div>
                    <asp:ValidationSummary ID="validSummary" runat="server" />
                     <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="Name is Required" ControlToValidate="txtName" Display="None"></asp:RequiredFieldValidator>
                     <asp:RequiredFieldValidator ID="rfvDesc" runat="server" ErrorMessage="Description is Required" ControlToValidate="txtDesc" Display="None"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rfvPrice" runat="server" ErrorMessage="Price is Required" ControlToValidate="txtPrice" Display="None"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rfvMenu" runat="server" ErrorMessage="Please Select a Menu" InitialValue="Please Choose One" Display="None" ControlToValidate="ddlMenu"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rfvCategory" runat="server" ErrorMessage="Please Select a Category" InitialValue="Please Choose One" Display="None" ControlToValidate="ddlCategory"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="rfvActive" runat="server" ErrorMessage="Please check Active Box" OnServerValidate="cvActive_ServerValidate" Display="None"></asp:CustomValidator>
                    <%--<asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer" ControlToValidate="txtPrice" ErrorMessage="Price in Numbers Only" Display="None" />--%>
                    <asp:RegularExpressionValidator ID="ValidPrice" runat="server" ErrorMessage="Invalid Price...Wrong Format" ValidationExpression="^\d+,\d{1,2}$" Display="None" ControlToValidate="txtPrice"></asp:RegularExpressionValidator>

                </div>
            </div>

        </div>
    </div>
</asp:Content>

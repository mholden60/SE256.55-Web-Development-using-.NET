﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;




    public class Menus
    {

        //Cannot remove image 
        public static List<MenuItem> GetMainMenu()
        {
        //The list of pages to populate the menu Nav bar
            List<MenuItem> items = new List<MenuItem>();
        //Default Page
        items.Add(new MenuItem("Homepage", "Home", "", "~/Default"));
        //Lunch Page
        items.Add(new MenuItem("LunchMenu", "LunchMenu", "", "~/LunchMenu"));
        //Dinner  Page
        items.Add(new MenuItem("DinnerMenu", "DinnerMenu", "", "~/DinnerMenu"));
        //Reservation Page
        items.Add(new MenuItem("Reservations", "Reservations", "", "~/Reservation"));
        //Directions Page
        items.Add(new MenuItem("Directions", "Directions", "", "~/Directions"));
        //About Page
        items.Add(new MenuItem("About", "About", "", "~/About"));
        //Contact Page
        items.Add(new MenuItem("Contact", "Contact", "", "~/Contact"));
        //Login Page
            items.Add(new MenuItem("Login", "Login", "", "~/Login"));
            return items;
        }
    }





















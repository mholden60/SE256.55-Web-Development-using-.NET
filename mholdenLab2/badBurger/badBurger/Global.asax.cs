﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Optimization;
using System.Web.Routing;

namespace badBurger
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes(RouteTable.Routes);
        }
        //Routes to make the URL user friendly
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapPageRoute("Default", "Default", "~/Default.aspx");
            routes.MapPageRoute("Lunchmenu", "LunchMenu", "~/LunchMenu.aspx");
            routes.MapPageRoute("Dinnermenu", "DinnerMenu", "~/DinnerMenu.aspx");
            routes.MapPageRoute("Reservations", "Reservations", "~/Reservations.aspx");
            routes.MapPageRoute("Reservation", "Reservation", "~/Reservation.aspx");
            routes.MapPageRoute("User", "User", "~/User.aspx");
            routes.MapPageRoute("Users", "Users", "~/Users.aspx");
            routes.MapPageRoute("Directions", "Directions", "~/Directions.aspx");
            routes.MapPageRoute("About", "About", "~/About.aspx");
            routes.MapPageRoute("Contact", "Contact", "~/Contact.aspx");
            routes.MapPageRoute("Login", "Login", "~/Login.aspx");
            routes.MapPageRoute("Forgotpassword", "ForgotPassword", "~/ForgotPassword.aspx");

        }
    }
}
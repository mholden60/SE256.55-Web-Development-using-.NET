﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Security.Authentication;
using System.Security.Cryptography;

namespace badBurger
{
    public class AppUser
    {
        public int UserId
        {
            get;
            set;
        }

        public string Email
        {
            get;
            set;
        }

        public string Salt
        {
            get;
            set;
        }

        public string FirstName
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }

        public string HashedPassword
        {
            get;
            set;
        }

        public bool ValidLogin
        {
            get;
            set;
        }

        #region Constructors

        public AppUser()
        {
            Salt = CreateSalt();

        }

        public AppUser(string Email)
        {
            DataTable dt = GetUser(Email);

            if (dt.Rows.Count > 0)
            {
                this.UserId = (int)dt.Rows[0]["user_id"];
                this.Email = dt.Rows[0]["user_email"].ToString();
                this.Salt = dt.Rows[0]["user_salt"].ToString();
                this.FirstName = dt.Rows[0]["user_first"].ToString();
                this.LastName = dt.Rows[0]["user_last"].ToString();
                this.HashedPassword = dt.Rows[0]["user_pwd"].ToString();
                this.ValidLogin = false;
            }

        }

        #endregion



        #region Methods/Functions
        private static string CreateSalt()
        {
            byte[] saltBytes = new byte[16];
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            rng.GetBytes(saltBytes);
            return Convert.ToBase64String(saltBytes);
        }

        private static DataTable GetUser(string Email)
        {
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["SE256_HoldenConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("users_getbyEmail", cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@user_email", SqlDbType.VarChar).Value = Email;
            DataTable dt = new DataTable();

            try
            {
                cn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception exc)
            {
                exc.ToString();
            }
            finally
            {
                cn.Close();
            }
            return dt;


        }

        private static string CreatePasswordHash(string salt, string pwd)
        {
            string saltAndPwd = string.Concat(salt, pwd);
            // Create a new instance of the hash crypto service provider.
            HashAlgorithm hashAlg = new SHA256CryptoServiceProvider();
            // Convert the data to hash to an array of Bytes.
            byte[] bytValue = System.Text.Encoding.UTF8.GetBytes(saltAndPwd);
            // Compute the Hash. This returns an array of Bytes.
            byte[] bytHash = hashAlg.ComputeHash(bytValue);
            // Optionally, represent the hash value as a base64-encoded string, 
            // For example, if you need to display the value or transmit it over a network.
            return Convert.ToBase64String(bytHash);
        }

        public static AppUser Login(string user_email, string user_pwd)
        {
            AppUser au = new AppUser();
            DataTable dt = GetUser(user_email);
            string auHash = CreatePasswordHash(dt.Rows[0]["user_salt"].ToString(), user_pwd);
            if (auHash == dt.Rows[0]["user_pwd"].ToString())
            {
                au = new AppUser(user_email);
                au.ValidLogin = true;
            }
            else au.ValidLogin = false;

            return au;
        }


        #endregion

        #region UserID interger

        #endregion


        #region Email String
        #endregion


        #region Salt String

        #endregion


        #region FirstName String
        #endregion


        #region LastName String
        #endregion


        #region HashedPassword
        #endregion


        #region ValidLogin Boolean

        #endregion
    }
}
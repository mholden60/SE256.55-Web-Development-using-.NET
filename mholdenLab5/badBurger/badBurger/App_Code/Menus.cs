﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;




    public class Menus
    {

        //Cannot remove image 
        public static List<MenuItem> GetMainMenu()
        {
        //The list of pages to populate the menu Nav bar
            List<MenuItem> items = new List<MenuItem>();
        //Default Page
        items.Add(new MenuItem("Homepage", "Home", "", "~/Default"));
        //Lunch Page
        items.Add(new MenuItem("LunchMenu", "LunchMenu", "", "~/LunchMenu"));
        //Dinner  Page
        items.Add(new MenuItem("DinnerMenu", "DinnerMenu", "", "~/DinnerMenu"));
        //Reservation Page
        items.Add(new MenuItem("Reservations", "Reservations", "", "~/Reservation"));
        //Directions Page
        items.Add(new MenuItem("Directions", "Directions", "", "~/Directions"));
        //About Page
        items.Add(new MenuItem("About", "About", "", "~/About"));
        //Contact Page
        items.Add(new MenuItem("Contact", "Contact", "", "~/Contact"));
        //Login Page
        items.Add(new MenuItem("Login", "Login", "", "~/Login"));
        //User Page
        items.Add(new MenuItem("ForgotPassword", "ForgotPassword", "", "~/ForgotPassword"));
        ////User Page
        //items.Add(new MenuItem("User", "Admin/User", "", "~/Admin/User"));
        ////Users Page
        //items.Add(new MenuItem("Users", "Admin/Users", "", "~/Admin/Users"));
        return items;
        }
    public static List<MenuItem> GetMainMenuAdmin()
    {
        //The list of pages to populate the Admin menu Nav bar
        List<MenuItem> AdminItems = new List<MenuItem>();
        //Admin MenuItems
        AdminItems.Add(new MenuItem("AMenuItems", "AMenuItems", "", "~/Admin/AMenuItems"));
        //Admin MenuItem
        AdminItems.Add(new MenuItem("AMenuItem", "Admin/AMenuItem", "", "~/Admin/AMenuItem"));
        //Admin Sections
        AdminItems.Add(new MenuItem("Sections", "/Admin/Sections", "", "~/Admin/Sections"));
        //Admin Section
        AdminItems.Add(new MenuItem("Section", "/Admin/Section", "", "~/Admin/Section"));
        //Admin Tables
        AdminItems.Add(new MenuItem("Tables", "/Admin/Tables", "", "~/Admin/Tables"));
        //Admin Table
        AdminItems.Add(new MenuItem("Table", "/Admin/Table", "", "~/Admin/Table"));
        //Admin Res
        AdminItems.Add(new MenuItem("ResMgmt", "/Admin/ResMgmt", "", "~/Admin/ResMgmt"));
        //User Page
        AdminItems.Add(new MenuItem("User", "/Admin/User", "", "~/Admin/User"));
        //Users Page
        AdminItems.Add(new MenuItem("Users", "/Admin/Users", "", "~/Admin/Users"));
        //Login Page
       // AdminItems.Add(new MenuItem("Login", "/Admin/Login", "", "~/Admin/Login"));

        return AdminItems;


    }
}




















